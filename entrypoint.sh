#!/bin/bash

trap "echo 'Exiting.'; chown -R root:root /home/git; exit" INT TERM

chmod 700 /home/git/.ssh
chown -R git:git /home/git
/usr/sbin/sshd -D -e
