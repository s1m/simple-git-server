# Simple Git Server

This project allows you to set a simple git server over ssh.

## Setup

You will need to set your public keys into data/.ssh/authorized_keys, following the example.

Then run :

```
podman run -ti --rm -v ./data:/home/git -p 2222:22 simple-git:latest
```

## Run

* To create a new repo:

```
ssh git@localhost -p 2222 new my-repo-name
```

* To clone :

```
git clone ssh://git@localhost:2222/~/my-repo-name.git
```
